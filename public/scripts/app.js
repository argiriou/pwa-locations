import installPWA from './installPWA.js';
import LocalNotification from './localNotification.js';
import Location from './location.js';
import Connection from './connection.js'


const SSIDS = [];

(function() {
  'use strict';
  
  if ('serviceWorker' in navigator) {
    console.log('navigator ',navigator)
    navigator.serviceWorker
             .register('./service-worker.js')
             .then(function() { console.log('Service Worker Registered'); });
  }
  
  /* Install PWA button */
  installPWA();
  /* ======================= */

  window.onload = function() {
    let ssid = localStorage.getItem("ssid") || 'Your ssid will display here';
    $('#ssid').html(ssid);
    $('#display').html(ssid);
  }

  $('#button').click(() => {
    console.log('click')
    let ssid = $('#ssid').val();
    console.log(ssid);
    $('#display').html(ssid);
    localStorage.setItem("ssid", ssid);
    
    if (ssid) {
      const title = `You have added "${ssid}" in the list`;
      // LocalNotification.sendNPNotif(title)
      //   .then(e => console.log(e, LocalNotification._permissionStatus()));  
      LocalNotification.sendNotif(title)
        .then(e => console.log(e, LocalNotification._permissionStatus()));  
    }
    
  });
  
  $('#locationButton').click(() => {
    
    Promise.resolve(confirm('I will request yout location! Is that ok?')).then(res => alert(res)).catch(er => alert(er));
    
    Location.loadPosition()
      .then((position) => {
        localStorage.setItem("position", JSON.stringify(position)) 
        alert(`Position ${position.latitude.toFixed(4)}, ${position.longitude.toFixed(4)} has been stored`)
        console.log(position);
        return position
      }).catch(er => console.warn('Error: loading position' ,er))

  });
  
  if (localStorage.getItem('locationScanning') === 'true' ? true : false) {
    document.getElementById("locationScanning").checked = true; 
  }
  
  $('#locationScanning').click(function() {
    localStorage.setItem('locationScanning', this.checked);
  })
  
  /* ======================= */
  /* ======= TODOs ========= */
  /* 
    1. Have an input that has names mapping to locations.
    2. The locations will be buttons that take current location (use Location WEB API)
    3. Store them to local Storage or Cache
    4. Whenever Connection type == "WIFI", then take the current location throught Location WEB API
    5. Calculate the distance with each stored location in our Storage.
    6. If distance closer than 100m???, then we know you are at that location
    7. Make an HTTP call to wherever we want
  */
  /* ======================= */
  
  
})();
