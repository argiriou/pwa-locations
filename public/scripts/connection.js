import LocalNotification from './localNotification.js';
import Location from './location.js';

export default (function() {
  const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
  let prevConType = connection.type;
  let currConType = null;

  const debouncedWrapper = debounced(updateConnectionStatus, 4000);
  connection.addEventListener('change', debouncedWrapper );
  
  /* FOR TESTING IN THE BROWSER */
  // window.addEventListener('offline', (e) => {
  //   navigator.connection.type = 'cellular';
  //   alert('offline ' + navigator.connection.type)
  // });
  /* ======================= */
  
  function updateConnectionStatus(e) {
    // e.stopPropagation();
    // e.preventDefault();
    currConType = connection.type;
    document.getElementById('networkType').innerHTML = `Changed from ${prevConType} to ${currConType}`;
    document.getElementById('effectiveNetworkType').innerHTML = connection.effectiveType;
    document.getElementById('downlinkMax').innerHTML = connection.downlinkMax;
    
    if (
      true
      // currConType === 'wifi' &&
      // prevConType !== 'wifi'
      /* TODO Have to check also if location is the same OR near, with same state */
    ) {
      const useLocationScanning = localStorage.getItem('locationScanning') === 'true' ? true : false;
      console.log(useLocationScanning)
      if (useLocationScanning) {
        Location.loadPosition()
          .then((fromPosition) => {
            console.log(fromPosition);
            return fromPosition
          })
          .then((fromPosition) => {
            const toPosition = JSON.parse(localStorage.getItem("position"));
            const distance = Math.round(Location.calcDistance(fromPosition, toPosition));
            console.log("distance ", distance)
            alert(distance);
            return distance;
          })
          .then((distance) => {
            if(distance < 100) {
              /* TODO have to set the state to this location now */
              return LocalNotification.sendNotif(
                `Welcome Home`,
                {
                  body: `Your distance is ${distance}m`
                }
              )
            } else {
              /* TODO have to Reset the state of this location */
              alert(`${distance} is too far`)
            }
          })
      }
    }
    /* Set prevConType to the current connection type */
    prevConType = connection.type;
  }
  
  function debounced(fn, delay) {
    let timerId;
    return function (...args) {
      if (timerId) {
        clearTimeout(timerId);
      }
      timerId = setTimeout(() => {
        fn(...args);
        timerId = null;
      }, delay);
    }
  }
  
}())