export default function() { 
  /* Install PWA button */
  const installBtnEl = document.getElementById('install-button');
  installBtnEl.style.display = 'none';
  let installPromptEvent;
  window.addEventListener('beforeinstallprompt', (event) => {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    event.preventDefault();
    // Stash the event so it can be triggered later.
    installPromptEvent = event;
    // Update the install UI to notify the user app can be installed
    installBtnEl.style.display = 'block';
  })

  installBtnEl.addEventListener('click', (e) => {
    // Update the install UI to remove the install button
    installBtnEl.style.display = 'none';
    // Show the modal add to home screen dialog
    installPromptEvent.prompt();
    // Wait for the user to respond to the prompt
    installPromptEvent.userChoice
      .then((choice) => {
        if (choice.outcome === 'accepted') {
          alert('User accepted the A2HS prompt');
        } else {
          alert('User dismissed the A2HS prompt');
        }
        // Clear the saved prompt since it can't be used again
        installPromptEvent = null;
      });
  });
  
  
  
    /* I have to try this --> */
  
//   window.addEventListener("beforeinstallprompt", event => {
//     // Suppress automatic prompting.
//     event.preventDefault();

//     // Show the (disabled-by-default) install button. This button
//     // resolves the installButtonClicked promise when clicked.
//     installButton.disabled = false;

//     // Wait for the user to click the button.
//     installButton.addEventListener("click", async e => {
//       // The prompt() method can only be used once.
//       installButton.disabled = true;

//       // Show the prompt.
//       const { userChoice } = await event.prompt();
//       console.info(`user choice was: ${userChoice}`);
//     });
//   });
  
  
}