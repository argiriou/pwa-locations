export default (function() {
  if (!('geolocation' in navigator)) { return {}};

  const geo_options = {
    enableHighAccuracy: true, 
    maximumAge        : 30000, 
    timeout           : 27000
  };
  
  return {
    getPosition,
    loadPosition,
    watchPosition,
    calcDistance
  }
  
  function geo_success(position) {
    console.log(position);
    console.log(position.coords.latitude, position.coords.longitude);
    return position;
  }

  function geo_error(er) {
    alert("Sorry, no position available.", er);
    return er;
  }
  
  function getPosition(options = {}) {
    // console.log(options && {...geo_options, ...options})
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  }
  
  async function loadPosition() {
    try {
      const location = await getPosition();
      console.log('location',location);
      const { latitude, longitude } = location.coords;
      return { latitude, longitude };
    } catch(err) {
      console.error("Error in::: loadPosition, ", err.message);
      // return Promise.reject(err);
    }
  }
  
  function watchPosition() {
    const watchId = navigator.geolocation.watchPosition(geo_success, geo_error);
    setTimeout(() => navigator.geolocation.clearWatch(watchId), 5000)
  }
  
  /* Calculate Distance GMaps */
  function calcDistance (
    { latitude: fromLat, longitude: fromLng },
    { latitude: toLat, longitude: toLng }
  ) {
    return google.maps.geometry.spherical.computeDistanceBetween(
      new google.maps.LatLng(fromLat, fromLng),
      new google.maps.LatLng(toLat, toLng)
    );
  }
}())