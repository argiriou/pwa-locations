export default (function() {
  const defaultOptions = {
    tag: 'notification',
    renotify: true,
    body: 'Here is my desription',
    icon: '../images/icons/icon-128x128.png',
    vibrate: [50, 100, 150],
  }

  return {
    sendNPNotif,
    sendNotif,
    _permissionStatus,
    _requestPermission,
    _nonPersistentNotification
  }
  function sendNPNotif(title) {
    return _requestPermission()
      .then(e => e ? alert(e) : null )
      .then(_nonPersistentNotification(title))
      .catch(e => console.log(e))
  }
  function sendNotif(title, options) {
    return _requestPermission()
      .then(e => e ? alert(e) : null )
      .catch(e => console.log(e))
      .then(_persistentNotification(title, options))
      .catch(e => console.log(e))
  }
  function _permissionStatus() { return Notification.permission };
  function _requestPermission() {
    if (!('Notification' in window)) {
      alert('Notification API not supported!');
      return Promise.reject('Not supported');
    }
    if (_permissionStatus() === 'granted') return Promise.reject('::Already granted');

    return Notification
      .requestPermission()
      .then((permission) => {
        if (permission !== 'granted') return Promise.reject('::Is denied');
        return permission;
      })
      .then((permission) => {
        // Handle granted permission here
        return permission;
      })
      .catch((err) => {
        console.log(err);
        return Promise.reject('::Is error denied');
        // Handle denied permission here
      });
  }

  function _nonPersistentNotification(title) {
    // if (!('Notification' in window)) {
    //   alert('Notification API not supported!');
    //   return;
    // }
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) return Promise.reject('::Is mobile device');
    console.log('_nonPersistentNotification')

    try {
      const notification = new Notification(title, defaultOptions);
      setTimeout(() => notification.close(), 2000);
      return notification;
    } catch (err) {
      alert('Notification API error: ' + err);
      return Promise.reject('::Notification API error: _nonPersistentNotification');
    }
  }

  function _persistentNotification(title, options) {
    const actions = [  
     {action: 'like', title: '👍Like'},  
     {action: 'reply', title: '↩ Reply'}
    ]
    options = {...defaultOptions, ...options}
    if (!('Notification' in window) || !('ServiceWorkerRegistration' in window)) {
      alert('Persistent Notification API not supported!');
      return;
    }

    try {
      navigator.serviceWorker.getRegistration()
        .then(reg => reg.showNotification(title, {actions ,...options}))
        .catch(err => {
          alert('Service Worker registration error: ' + err)
          return Promise.reject('::Service Worker registration error');
        });
    } catch (err) {
      alert('Notification API error: ' + err);
      return Promise.reject('::Notification API error: _persistentNotification');
    }
  }

}());