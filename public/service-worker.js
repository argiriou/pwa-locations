var dataCacheName = 'template-pwa';
var cacheName = 'template-pwa';
var filesToCache = [
  // '/public/',
 // "./public/fonts",
 // "./public/fonts/roboto",
 // "./public/fonts/roboto/Roboto-Bold.woff",
 // "./public/fonts/roboto/Roboto-Bold.woff2",
 // "./public/fonts/roboto/Roboto-Light.woff",
 // "./public/fonts/roboto/Roboto-Light.woff2",
 // "./public/fonts/roboto/Roboto-Medium.woff",
 // "./public/fonts/roboto/Roboto-Medium.woff2",
 // "./public/fonts/roboto/Roboto-Regular.woff",
 // "./public/fonts/roboto/Roboto-Regular.woff2",
 // "./public/fonts/roboto/Roboto-Thin.woff",
 // "./public/fonts/roboto/Roboto-Thin.woff2",
 // "./public/images",
 // "./public/images/icons",
 "./images/icons/icon-128x128.png",
 "./images/icons/icon-144x144.png",
 "./images/icons/icon-152x152.png",
 "./images/icons/icon-192x192.png",
 "./images/icons/icon-256x256.png",
 "./index.html",
 "./manifest.json",
 // "./scripts",
 "./scripts/app.js",
 "./scripts/jquery-3.3.1.js",
 // "./scripts/materialize.js",
 "./service-worker.js",
 // "./styles",
 "./styles/materialize-new.css",
 "./styles/style.css"
];
console.log(self)
self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    }).catch(e => console.log('install: ', e))
  );
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    }).catch(e => console.log('activate: ', e))
  );
  return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
  console.log('[Service Worker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    }).catch(e => console.log('fetch: ', e))
  );
});

self.addEventListener('notificationclick', function(event) {  
  var messageId = event.notification.data;

  event.notification.close();
  console.log("AAAAAAAAAAAA",event.action);
  if (event.action === 'like') {  
    // silentlyLikeItem();
  }  
  else if (event.action === 'reply') {  
    // clients.openWindow("/messages?reply=" + messageId);  
  }  
  else {  
    // clients.openWindow("/messages?reply=" + messageId);  
  }  
}, false);

